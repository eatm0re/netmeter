package net.edubovit.netmeter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Random;

public class Main {

    private static final Random RANDOM = new Random(666);

    private static int port = 20001;

    private static int packetSize = 100 * 1024;

    private static long outputInterval = 5;

    private static String workload = "noop";

    public static void main(String[] args) throws IOException {
        parseOptions(args);

        if ("server".equalsIgnoreCase(args[0])) {
            var server = new ServerSocket(port);
            System.out.println("Ready.");
            var socket = server.accept();
            System.out.format("Connection from %s%n", socket.getInetAddress());
            var inputStream = socket.getInputStream();
            long packetsReceived = 0;
            long packetsReceivedLast = 0;
            long timeStart = System.nanoTime();
            long timeLast = timeStart;
            byte[] sample = new byte[packetSize];
            while (true) {
                if (!"noop".equals(workload)) {
                    fill(sample);
                }
                byte[] payload = inputStream.readNBytes(packetSize);
                packetsReceived++;
                long timeNow = System.nanoTime();
                if (!"noop".equals(workload) && !Arrays.equals(sample, payload)) {
                    System.err.println("Wrong payload received");
                    break;
                } else if (timeNow - timeLast > outputInterval * 1_000_000_000) {
                    double timeFromLast = (timeNow - timeLast) / 1_000_000_000.0;
                    timeLast = timeNow;
                    double timeTotal = (timeNow - timeStart) / 1_000_000_000.0;
                    System.out.format("Packets received: %d, %.0f seconds, %.2f MB/s, last %.2f MB/s%n",
                            packetsReceived, timeTotal,
                            packetsReceived * packetSize / timeTotal / (1024 * 1024),
                            (packetsReceived - packetsReceivedLast) * packetSize / timeFromLast / (1024 * 1024));
                    packetsReceivedLast = packetsReceived;
                }
            }
        } else {
            String host = args[0];
            if (host.contains(":")) {
                String[] splitted = host.split(":", 2);
                host = splitted[0];
                port = Integer.parseInt(splitted[1]);
            }
            var socket = new Socket(host, port);
            var outputStream = socket.getOutputStream();
            byte[] payload = new byte[packetSize];
            while (true) {
                fill(payload);
                outputStream.write(payload);
            }
        }
    }

    public static void fill(byte[] bytes) {
        switch (workload) {
            case "noop" -> fillNoop(bytes);
            case "seq" -> fillSeq(bytes);
            case "random" -> fillRandom(bytes);
        }
    }

    public static void fillNoop(byte[] bytes) {
        // no-op
    }

    public static void fillSeq(byte[] bytes) {
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) i;
        }
    }

    public static void fillRandom(byte[] bytes) {
        RANDOM.nextBytes(bytes);
    }

    public static void parseOptions(String[] args) {
        for (int i = 0; i < args.length - 1; i++) {
            switch (args[i]) {
                case "-p" -> port = parsePort(args[i + 1]);
                case "-s" -> packetSize = parsePacketSize(args[i + 1]);
                case "-i" -> outputInterval = parseOutputInterval(args[i + 1]);
                case "-w" -> workload = parseWorkload(args[i + 1]);
            }
        }
    }

    public static int parsePort(String value) {
        return Integer.parseInt(value);
    }

    public static int parsePacketSize(String value) {
        int result;
        switch (value.substring(value.length() - 1).toUpperCase()) {
            case "K" -> result = Integer.parseInt(value.substring(0, value.length() - 1)) * 1024;
            case "M" -> result = Integer.parseInt(value.substring(0, value.length() - 1)) * 1024 * 1024;
            case "G" -> result = Integer.parseInt(value.substring(0, value.length() - 1)) * 1024 * 1024 * 1024;
            default -> result = Integer.parseInt(value.substring(0, value.length() - 1));
        }
        return result;
    }

    public static int parseOutputInterval(String value) {
        return Integer.parseInt(value);
    }

    public static String parseWorkload(String value) {
        return value.toLowerCase();
    }

    public static String printSomeElements(byte[] array) {
        var result = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            result.append(array[i]);
            result.append(' ');
        }
        result.append("... ");
        for (int i = array.length / 2 - 2; i < array.length / 2 + 2; i++) {
            result.append(array[i]);
            result.append(' ');
        }
        result.append("... ");
        for (int i = array.length - 5; i < array.length; i++) {
            result.append(array[i]);
            result.append(' ');
        }
        return result.toString();
    }

}
